// ---------------
// test_Grades.cpp
// ---------------

// --------
// includes
// --------

#include <map>    // map
#include <string> // string
#include <vector> // vector

#include "gtest/gtest.h"

#include "Grades.hpp"

using namespace std;

// ----------------
// test_grades_eval
// ----------------

TEST(grades_eval, test_0) {
    const vector<vector<int>> v_v_scores = {{2, 2, 2, 2, 0},
        {2, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 0},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3},
        {2, 2, 2, 1, 2, 2, 2, 2, 2, 3, 2, 3, 0, 0},
        {2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 3, 2, 3, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B-");
}

TEST(grades_eval, test_1) {
    const vector<vector<int>> v_v_scores = {{2, 1, 2, 3, 3},
        {2, 2, 1, 2, 2, 2, 2, 2, 2, 3, 3, 0},
        {3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3},
        {2, 2, 2, 1, 2, 2, 2, 2, 2, 3, 2, 3, 0, 3},
        {3, 2, 2, 0, 1, 2, 2, 1, 2, 1, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 3, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 0}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "A");
}

TEST(grades_eval, test_2) {
    const vector<vector<int>> v_v_scores = {{3, 3, 3, 1, 3},
        {2, 2, 1, 2, 2, 2, 2, 2, 2, 3, 3, 1},
        {3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 3, 3, 3},
        {3, 2, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 2, 3, 2, 2, 2, 0, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 3, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 1}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "A-");
}

TEST(grades_eval, test_3) {
    const vector<vector<int>> v_v_scores = {{2, 2, 1, 2, 3},
        {2, 3, 3, 2, 2, 2, 2, 2, 2, 3, 3, 2},
        {3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3},
        {3, 3, 2, 1, 2, 2, 2, 2, 2, 3, 2, 3, 1, 3},
        {2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 3, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 3, 2, 3}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B+");
}

TEST(grades_eval, test_4) {
    const vector<vector<int>> v_v_scores = {{3, 3, 3, 2, 3},
        {1, 2, 2, 2, 1, 2, 2, 2, 2, 3, 3, 0},
        {2, 0, 2, 2, 2, 2, 2, 2, 1, 3, 2, 3, 2, 2},
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 2, 2, 2},
        {3, 2, 2, 2, 0, 2, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 0, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B");
}

TEST(grades_eval, test_5) {
    const vector<vector<int>> v_v_scores = {{2, 2, 2, 2, 0},
        {2, 0, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2},
        {2, 2, 2, 0, 2, 2, 2, 2, 1, 3, 2, 3, 1, 1},
        {2, 3, 2, 2, 2, 2, 2, 2, 2, 1, 0, 3, 0, 0},
        {3, 3, 2, 0, 2, 2, 1, 1, 2, 2, 2, 2, 2, 3, 2, 2, 2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 2, 2, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B-");
}

TEST(grades_eval, test_6) {
    const vector<vector<int>> v_v_scores = {{1, 2, 2, 2, 2},
        {2, 0, 2, 1, 2, 2, 2, 2, 2, 2, 0, 2},
        {2, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2},
        {2, 2, 3, 2, 2, 3, 2, 2, 2, 2, 2, 3, 3, 2},
        {3, 2, 1, 2, 2, 2, 3, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 2, 2, 2, 3, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C+");
}

TEST(grades_eval, test_7) {
    const vector<vector<int>> v_v_scores = {{1, 1, 3, 3, 3},
        {2, 0, 2, 1, 2, 2, 2, 2, 2, 2, 0, 2},
        {2, 3, 3, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2},
        {1, 1, 3, 2, 0, 3, 2, 2, 2, 2, 2, 1, 0, 2},
        {2, 2, 2, 3, 2, 0, 2, 2, 2, 2, 0, 2, 2, 1, 3, 2, 2, 2, 2, 2, 1, 2, 2, 0, 2, 2, 2, 2, 3, 2, 2, 2, 3, 2, 0, 2, 2, 1, 3, 2, 2, 0}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C");
}

TEST(grades_eval, test_8) {
    const vector<vector<int>> v_v_scores = {{3, 3, 3, 3, 3},
        {1, 3, 3, 1, 3, 3, 2, 2, 1, 1, 1, 1},
        {1, 3, 3, 1, 3, 3, 3, 3, 2, 2, 3, 3, 1, 2},
        {3, 3, 3, 2, 3, 3, 2, 1, 2, 2, 3, 1, 3, 3},
        {3, 2, 1, 3, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3, 3, 2, 0, 3, 2, 2, 2, 2, 2, 0, 2, 2, 1, 2, 3, 2, 3, 2, 3, 2, 2, 2, 2, 3, 3, 2, 2, 1}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "C-");
}

TEST(grades_eval, test_9) {
    const vector<vector<int>> v_v_scores = {{2, 2, 1, 3, 1},
        {2, 1, 3, 1, 3, 2, 1, 3, 2, 2, 0, 0},
        {2, 2, 1, 2, 0, 2, 1, 2, 2, 0, 2, 2, 1, 2},
        {1, 2, 0, 2, 2, 2, 1, 2, 2, 0, 2, 2, 1, 2},
        {0, 0, 0, 0, 2, 2, 2, 3, 2, 1, 3, 3, 2, 2, 0, 2, 2, 2, 3, 3, 2, 2, 2, 2, 1, 2, 1, 2, 0, 2, 2, 2, 1, 1, 2, 2, 2, 1, 2, 0, 2, 0}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D+");
}

TEST(grades_eval, test_10) {
    const vector<vector<int>> v_v_scores = {{2, 2, 2, 1, 1},
        {2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 0, 2},
        {2, 2, 1, 2, 0, 2, 1, 2, 2, 0, 2, 2, 1, 2},
        {2, 0, 2, 0, 2, 2, 0, 2, 2, 2, 0, 2, 0, 2},
        {2, 0, 2, 2, 2, 0, 2, 0, 2, 2, 1, 2, 2, 1, 1, 2, 2, 0, 0, 2, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 1, 2, 0, 2, 1, 2, 2, 2, 2, 0, 2, 2}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D");
}

TEST(grades_eval, test_11) {
    const vector<vector<int>> v_v_scores = {{3, 3, 2, 1, 3},
        {2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 3, 2},
        {2, 2, 3, 2, 3, 2, 3, 2, 2, 3, 2, 2, 1, 2},
        {2, 3, 2, 1, 2, 2, 1, 2, 2, 2, 3, 2, 3, 2},
        {2, 0, 2, 2, 2, 0, 2, 0, 2, 2, 1, 2, 0, 1, 1, 2, 2, 0, 0, 2, 2, 2, 2, 2, 0, 0, 2, 2, 2, 0, 1, 2, 0, 2, 1, 2, 2, 2, 2, 0, 2, 2}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D-");
}

TEST(grades_eval, test_12) {
    const vector<vector<int>> v_v_scores = {{2, 1, 1, 1, 3},
        {2, 2, 1, 2, 2, 2, 2, 2, 2, 3, 3, 1},
        {3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3},
        {1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 3, 3, 3},
        {3, 2, 2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 2, 3, 2, 2, 2, 0, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 3, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3, 1}
    };
    const string letter = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "F");
}

TEST(get_total_scores, test_13) {
    const vector<vector<int>> v_v_scores = {{2, 1, 1, 3, 3},
        {2, 0, 2, 1, 2, 3, 2, 0, 2, 3, 2, 3},
        {3, 3, 3, 1, 2, 1, 2, 2, 2, 3, 2, 2, 1, 3},
        {0, 0, 2, 1, 2, 2, 2, 2, 2, 3, 2, 2, 1, 3},
        {2, 2, 2, 2, 1, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 1, 3, 2, 2, 2, 1, 2, 3}
    };
    const vector<int> scores = get_total_scores(v_v_scores);
    const vector<int> expected = {4, 10, 13, 11, 41};
    ASSERT_EQ(scores, expected);
}

TEST(get_total_scores, test_14) {
    const vector<vector<int>> v_v_scores = {{2, 2, 3, 1, 1},
        {2, 2, 2, 1, 2, 2, 2, 2, 2, 3, 1, 3},
        {2, 2, 1, 2, 0, 2, 1, 2, 2, 0, 2, 2, 1, 2},
        {2, 1, 2, 0, 2, 2, 1, 2, 2, 2, 0, 3, 0, 2},
        {3, 0, 2, 3, 2, 0, 2, 0, 2, 3, 1, 2, 2, 1, 1, 1, 2, 0, 0, 2, 2, 2, 2, 1, 0, 0, 2, 2, 2, 0, 1, 1, 0, 1, 1, 2, 2, 2, 3, 0, 3, 3}
    };
    const vector<int> scores = get_total_scores(v_v_scores);
    const vector<int> expected = {3, 11, 9, 9, 26};
    ASSERT_EQ(scores, expected);
}

TEST(get_total_scores, test_15) {
    const vector<vector<int>> v_v_scores = {{3, 3, 1, 2, 3},
        {3, 2, 1, 2, 2, 0, 2, 0, 2, 3, 3, 1},
        {3, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 3},
        {1, 0, 2, 0, 2, 2, 2, 2, 2, 1, 2, 3, 3, 3},
        {0, 2, 3, 1, 1, 2, 2, 1, 2, 1, 1, 2, 2, 3, 2, 2, 1, 1, 2, 2, 2, 3, 2, 2, 2, 3, 2, 2, 3, 0, 3, 2, 2, 2, 3, 2, 2, 2, 2, 0, 3, 1}
    };
    const vector<int> scores = get_total_scores(v_v_scores);
    const vector<int> expected = {5, 9, 13, 11, 35};
    ASSERT_EQ(scores, expected);
}