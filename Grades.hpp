// ----------
// Grades.hpp
// ----------

#ifndef Grades_hpp
#define Grades_hpp

// --------
// includes
// --------

#include <algorithm> // count, min_element, transform
#include <cassert>   // assert
#include <map>       // map
#include <string>    // string
#include <vector>    // vector

using namespace std;

// -----------
// get_total_scores - Helper method for calculating total scores for each category
// -----------

vector<int> get_total_scores(const vector<vector<int>>& v_v_scores) {
    const vector<int> assert_size_checks = {5, 12, 14, 14, 42}; // number of scores of categories
    vector<int> total_scores; // stores students scores for each category
    for (int i = 0; i < v_v_scores.size(); ++i) {
        int three_count = 0;
        int one_count = 0;
        int total_score = 0; // number of 2s and 3s
        assert(v_v_scores[i].size() == assert_size_checks[i]);
        for (int j = 0; j < v_v_scores[i].size(); ++j) {
            int score = v_v_scores[i][j];
            assert(score >= 0 && score <= 3);
            switch (score) {
            case 1:
                ++one_count;
                break;
            case 2:
                ++total_score;
                break;
            case 3:
                ++total_score;
                ++three_count;
                break;
            }
        }
        int makeup = min(one_count, three_count / 2); // Two 3s replace a 1, this calculates how many 1s get replaced
        total_score += makeup; // Add replaced 1s to the score
        total_scores.push_back(total_score);
    }
    assert(total_scores.size() == v_v_scores.size());
    return total_scores;
}

// -----------
// grades_eval - Returns letter grade based on student's scores (vector of 5 vectors)
// -----------

string grades_eval (const vector<vector<int>>& v_v_scores) {
    assert(&v_v_scores);
    assert(!v_v_scores.empty());
    assert(v_v_scores.size() == 5);
    const vector<string> letter_grades = {"A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"};
    const vector<int> projects = {5, 5, 4, 4, 4, 4, 4, 4, 3, 3, 3, 0};
    const vector<int> exercises = {11, 11, 10, 10, 10, 9, 9, 8, 8, 8, 7, 0};
    const vector<int> blogs = {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0};
    const vector<int> papers = {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8, 0};
    const vector<int> quizzes = {39, 38, 37, 35, 34, 32, 31, 29, 28, 27, 25, 0};
    const vector<vector<int>> all_categories = {projects, exercises, blogs, papers, quizzes};
    vector<int> total_scores = get_total_scores(v_v_scores); // Retrieve student's scores for all categories
    int grade_index = 0;
    for (int i = 0; i < total_scores.size(); ++i) {
        int score = total_scores[i];
        const vector<int> category_scores = all_categories[i];
        for (int j = 0; j < category_scores.size(); ++j) {
            if (score >= category_scores[j]) { // Find the index of the associated grade for that category's score
                grade_index = max(grade_index, j); // Keep track of the highest index / lowest letter grade
                break;
            }
        }
    }
    assert(grade_index >= 0 && grade_index < letter_grades.size());
    return letter_grades[grade_index]; // Index into list of letter grades based on lowest scored category
}

#endif // Grades_hpp
